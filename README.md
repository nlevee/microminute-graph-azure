# microminute-graph-azure

## Création de l'app registration sur Azure

Sur le portail Azure, dans App registrations : 
https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade

Click sur 'New registrations'

Compléter les champs requis : 
Name : libre
Account Types : 'Accounts in this organizational directory only (Répertoire par défaut only - Single tenant)'

Copier : 
Application (client) ID : `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`
Directory (tenant) ID : `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`

Dans 'Certificates & secrets', click sur 'New client secret', Ajouter une description et Cocher Expires 'Never'

Copier le Client Secret : `XxXXxXxXxxXXXxxXxxXxXxXXXXx`

Dans 'API permissions' ajouter la permission 'User.Read.All'

Puis cliquer sur 'Grant admin consent for ...'

## Configuration

Créer un fichier `config.php` à partir du fichier `config-sample.php`.

Puis compléter les informations du fichier avec les données copier depuis l'interface Azure : 

```php
<?php

$AZ_CRENDENTIALS = [
    'client_id' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
    'client_secret' => 'XxXXxXxXxxXXXxxXxxXxXxXXXXx',
    'tenant_id' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
];
```

## Utilisation

Il y a 2 mode prévue pour la sortie des informations de l'API Azure : 

Dans le fichier `az-graph.php`, mettre en commentaire le mode non souhaité :

```php
// serialize user list with filtered params
$users = serializeUserList($azUsers, 'serializeUserProperties', [[
    'displayName',
    'givenName',
    'surname',
    'mail',
]]);

// serialize userlist with all params
$users = serializeUserList($azUsers, 'serializeUserAllProperties');
```

Puis lancer le script :

```sh
php -f az-graph.php
```

La sortie du script est en JSON, encodé en UTF-8