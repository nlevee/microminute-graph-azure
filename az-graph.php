<?php

// load autoload file from composer
require __DIR__ . '/vendor/autoload.php';

// read config file
require __DIR__ . '/config.php';

/**
 * Get access token from az graph oauth2 endpoint
 * @param array $config config array
 * @return string
 */
function azGetAccessToken(array $config) :string 
{
    try {
        // call oauth2 endpoint to get access token
        $guzzle = new \GuzzleHttp\Client();
        $url = 'https://login.microsoftonline.com/' . $config['tenant_id'] . '/oauth2/v2.0/token';
        $response = $guzzle->post(
            $url, 
            [
                'form_params' => [
                    'client_id' => $config['client_id'],
                    'client_secret' => $config['client_secret'],
                    'scope' => 'https://graph.microsoft.com/.default',
                    'grant_type' => 'client_credentials',
                ],
            ]
        );
        $token = json_decode($response->getBody()->getContents(), true);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        $errorContent = json_decode($e->getResponse()->getBody()->getContents(), true);
        throw new \Exception("azGetAccessToken : " . $errorContent['error_description'], 0, $e);
    }
    
    return $token['access_token'];
}

/**
 * Get Users list from AZ Graph API
 * @param string $accessToken Token to call AZ API
 * @return array
 */
function azGetUsers(string $accessToken):array
{
    $graph = new \Microsoft\Graph\Graph();
    $graph->setAccessToken($accessToken);
    $users = $graph->createRequest("GET", "/users")
        ->setReturnType(\Microsoft\Graph\Model\User::class)
        ->execute();
        
    return $users;
}

/**
 * Serialize all user properties to array 
 * @param \Microsoft\Graph\Model\User $user
 * @return array
 */
function serializeUserAllProperties(\Microsoft\Graph\Model\User $user):array 
{ 
    return $user->getProperties();
}

/**
 * Serialize some user properties filtered by $propNames to array 
 * @param \Microsoft\Graph\Model\User $user
 * @param array $propNames
 * @return array
 */
function serializeUserProperties(\Microsoft\Graph\Model\User $user, array $propNames):array
{ 
    $allProperties = $user->getProperties();
    return array_intersect_key($allProperties, array_flip($propNames));
}

/**
 * Serialize a graph user list with a serializer
 * @param \Microsoft\Graph\Model\User[] $userlist
 * @param callable $serializer function to call to serialize each user model
 * @param array $serializerArgs arguments to pass to serializer
 * @return array
 */
function serializeUserList(array $userlist, callable $serializer, array $serializerArgs = []) :array 
{
    // apply serialize function on all user in list
    $users = array_map(
        function ($user) use ($serializer, $serializerArgs) :array {
            // ajoute en premier argument le user
            $args = $serializerArgs;
            array_unshift($args, $user);
            return call_user_func_array($serializer, $args);
        },
        $userlist
    );

    return $users;
}

// get access token
$accessToken = azGetAccessToken($AZ_CRENDENTIALS);

// get user list
$azUsers = azGetUsers($accessToken);

// serialize user list with filtered params
$users = serializeUserList($azUsers, 'serializeUserProperties', [[
    'displayName',
    'givenName',
    'surname',
    'mail',
]]);

// serialize userlist with all params
$users = serializeUserList($azUsers, 'serializeUserAllProperties');

// convert list to json
echo json_encode($users);